from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from .models import Project
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.
class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    # fields = ["name", "description", "members"]
    template_name = "projects/detail.html"

    # def get_success_url(self):
    #     return reverse_lazy("show_project", args=[self.object.id])


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    fields = ["name", "description", "members"]
    template_name = "projects/create.html"

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])
